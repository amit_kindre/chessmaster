import time
import serial

ser = serial.Serial(
    # port='/dev/ttyUSB1',
    port='COM2',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

try:
    ser.isOpen()
except:
    print('Port Open Error')

while True:
    ser.write(b'hello')
    time.sleep(1.0)
