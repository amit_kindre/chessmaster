from Chessnut import Game

chessgame = Game()
print (chessgame)  # 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'
print (chessgame.get_moves())
chessgame.apply_move('e2e4')
print(chessgame)
print (chessgame.get_moves())
chessgame.apply_move('e7e5')
print(chessgame)
print (chessgame.get_moves())
chessgame.apply_move('f1c4')
print(chessgame)
print (chessgame.get_moves())
chessgame.apply_move('b8c6')
print(chessgame)
print (chessgame.get_moves())

chessgame.apply_move('d1h5')
print(chessgame)
print (chessgame.get_moves())

chessgame.apply_move('d7d6')
print(chessgame)
print (chessgame.get_moves())

chessgame.apply_move('h5f7')
print(chessgame)
print (chessgame.get_moves())

