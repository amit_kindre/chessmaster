# import os
import speech_recognition as sr
from Chessnut import Game
import time
import serial
import winsound
# import pygame
import pyglet

# pygame.init()
# pygame.mixer.init()
# pygame.mixer.init()
# pygame.mixer.music.set_volume(1)
# pygame.mixer.music.load('t1.wav')
# pygame.mixer.music.play()
# while pygame.mixer.music.get_busy()==True:
# 	continue
	



def errorSound():
	# winsound.Beep(600, 100) # frequency, duration
	# time.sleep(0.1)        
	# winsound.Beep(600, 100) # frequency, duration
	# time.sleep(0.1)        
	# winsound.Beep(600, 100) # frequency, duration
	
	music = pyglet.resource.media('t1.wav')
	music.play()
	time.sleep(0.5)
	music = pyglet.resource.media('t1.wav')
	music.play()
	time.sleep(0.5)
	return

def okSound():
	# winsound.Beep(600, 200) # frequency, duration

	music = pyglet.resource.media('t1.wav')
	music.play()
	return

def checkMateSound():
	music = pyglet.resource.media('t3.wav')
	music.play()
	return

ser = serial.Serial(
    # port='/dev/ttyUSB1',
	port='COM4',
	baudrate=9600,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS
	)

music = pyglet.resource.media('t1.wav')
music.play()


try:
    ser.isOpen()
    print('Port Open')
except:
    print('Port Open Error')

chessgame = Game()
print (chessgame) 

r = sr.Recognizer()
m = sr.Microphone()

with  m as source:
	r.adjust_for_ambient_noise(source)
	print('Welcome To Chess')
	
	while True:	
		okSound()
	
		validMoves = chessgame.get_moves()
		## checkmate conition check
		if(len(validMoves) < 3):
			print('check mate')
			checkMateSound()
			# return

		Go = input("Ready To Go?")
		print (validMoves)
		okSound()
		print('Speak Source Block')
		audio = r.listen(source)
		print('Converting')
		try:
			text_s = r.recognize_google(audio)
			# text_ss = r.recognize_sphinix(audio)
			print('Source : {}'.format(text_s))
			# print('Source : {}'.format(text_ss))
			
			if (len(text_s) != 2):
				errorSound()
				print('Invalid input')
				continue

		except:
			errorSound()
			print('Sorry Coud not recognize voice')
			continue
		okSound()
		print('Speak Destination Block')
		audio = r.listen(source)
		print('Converting')
		try:
			text_d = r.recognize_google(audio)
			# text_ds = r.recognize_sphinix(audio)
			print('Destination : {}'.format(text_d))
			if (len(text_d) != 2):
				errorSound()
				print('Invalid input')
				continue

		except:
			errorSound()
			print('Sorry Coud not recognize voice')
			continue
	
		print("source destination %s-%s" % (text_s,text_d))
		try:
			move_serial_str = text_s[1] + text_s[0] + text_d[1] + text_d[0]
			x1_num = int(text_s[0]) + 0x40
			x1_chr = chr(x1_num)
			x2_num = int(text_d[0]) + 0x40
			x2_chr = chr(x2_num)
			moveString = x1_chr + text_s[1] + x2_chr + text_d[1]
			print("source destination %s" % (moveString))
			chessgame.apply_move(moveString)
			print(chessgame)
			ser.write((move_serial_str + '\r\n').encode('utf-8'))
		except Exception as e:	
			winsound.Beep(440, 500) # frequency, duration
			time.sleep(0.50)        			
			print('Move again %s' %(e))



